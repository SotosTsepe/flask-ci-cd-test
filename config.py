import os
from base64 import b64encode

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or b64encode(os.urandom(32)).decode()
    ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD')
    if not ADMIN_PASSWORD:  # pragma: no cover
        raise RuntimeError('Administrator password has not been configured.')


class TestConfig(Config):
    TESTING = True
