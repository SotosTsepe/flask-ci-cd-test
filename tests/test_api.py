from urllib.parse import urlencode


def call(client, path, params):
    url = path + '?' + urlencode(params)
    return client.get(url)


def test_index(client):
    response = call(client, '/', {})
    assert response.status_code == 302


def test_api_index(client):
    response = call(client, '/api', {})
    assert response.status_code == 200
    assert response.json['status'] == 200
    assert 'links' in response.json


def test_addition(client):
    response = call(client, '/api/addition', {})
    assert response.status_code == 400
    assert response.json['status'] == 400

    response = call(client, '/api/addition', {'a': 'x', 'b': 'y'})
    assert response.status_code == 400
    assert response.json['status'] == 400

    response = call(client, '/api/addition', {'a': 2, 'b': 3.5})
    assert response.status_code == 200
    assert response.json['status'] == 200
    assert response.json['result'] == 5.5


def test_square(client):
    response = call(client, '/api/square', {})
    assert response.status_code == 400
    assert response.json['status'] == 400

    response = call(client, '/api/square', {'a': 'x', 'b': 'y'})
    assert response.status_code == 400
    assert response.json['status'] == 400

    response = call(client, 'api/square', {'a': 10, 'b': 3})
    assert response.status_code == 200
    assert response.json['status'] == 200
    assert response.json['result'] == 1000
