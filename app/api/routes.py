from flask import request, url_for

from app.api import api_bp


@api_bp.route('')
def index():
    return {
        'message': 'Success',
        'status': 200,
        'links': [
            url_for('api.addition', _external=True),
            url_for('api.square', _external=True)
        ]
    }


@api_bp.route('/addition')
def addition():
    if not 'a' or 'b' not in request.args:
        return {
            'message': 'Missing arguments. Required a and b in url parameters.',
            'status': 400
        }, 400
    try:
        a = float(request.args['a'])
        b = float(request.args['b'])
    except:
        return {
            'message': 'Wrong arguments type. Url parameters must be either integer or float type.',
            'status': 400
        }, 400
    return {
        'message': 'Success.',
        'status': 200,
        'result': a + b
    }


@api_bp.route('/square')
def square():
    if not 'a' or 'b' not in request.args:
        return {
            'message': 'Missing arguments. Required a and b in url parameters.',
            'status': 400
        }, 400
    try:
        a = float(request.args['a'])
        b = float(request.args['b'])
    except:
        return {
            'message': 'Wrong arguments type. Url parameters must be either integer or float type.',
            'status': 400
        }, 400
    return {
        'message': 'Success.',
        'status': 200,
        'result': a ** b
    }